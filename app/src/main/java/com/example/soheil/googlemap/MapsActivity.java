package com.example.soheil.googlemap;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.soheil.yahoomodel.YahooModels;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    private GoogleMap mMap;
    TextView location, temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        bind();
    }

    void bind() {
        location = findViewById(R.id.location);
        temp = findViewById(R.id.temp);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng tehran = new LatLng(35.7108425, 51.4164263);
//        mMap.addMarker(new MarkerOptions().position(tehran).title("Marker in Tehran"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(tehran));
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                tehran, 15);
        mMap.animateCamera(location);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnCameraIdleListener(this);
    }

    void getWeatherByLocation(double lat, double lng) {
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather." +
                "forecast%20where%20woeid%20in%20(SELECT%20woeid%20FROM%20geo.places%20WHERE%20" +
                "text%3D%22(" + lat + "%2C%20" + lng + ")%22)and%20u%3D%22c%22&format=json&env=store%" +
                "3A%2F%2Fdatatables.org%2Falltableswithkeys";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(MapsActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseData(responseString);
            }
        });

    }

    private void parseData(String response) {
        try {
            Gson gson = new Gson();
            YahooModels yahoo = gson.fromJson(response, YahooModels.class);
            if (yahoo.getQuery().getCount() == 1) {
                String temp = yahoo.getQuery().getResults().getChannel().getItem()
                        .getCondition().getTemp();
                String location = yahoo.getQuery().getResults().getChannel().getLocation().getCity();
                this.temp.setText(temp);
                this.location.setText(location);
            } else
                Toast.makeText(this, "failed", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            Toast.makeText(this, "Exceptions", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onCameraIdle() {
        double latitude = mMap.getCameraPosition().target.latitude;
        double longitude = mMap.getCameraPosition().target.longitude;
        getWeatherByLocation(latitude, longitude);
//        Toast.makeText(this, "Lat:" + latitude + "Lon:" + longitude, Toast.LENGTH_SHORT).show();

    }
}
